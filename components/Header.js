import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
} from "reactstrap";

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [sticky, setSticky] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const pesan = useSelector((state) => state.pesan);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  });

  const handleScroll = () => {
    if (window.scrollY > 90) {
      setSticky(true);
    } else if (window.scrollY < 90) {
      setSticky(false);
    }
  };

  return (
    <div className={`header${sticky ? " sticky" : ""}`}>
      <Navbar light expand="md">
        <Container>
          <NavbarBrand href="/">Dashboard Covid-19</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav navbar>
              <NavItem>
                <NavLink href="/">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/provinsi">Provinsi</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/global">Global</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/video">Video</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/article">Article</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/editor">Text Editor</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
          <NavbarText className="text-info">
            <b>{pesan}</b>
          </NavbarText>
        </Container>
      </Navbar>
    </div>
  );
};

export default Header;
