import Header from "../components/Header";
import Layout from "../components/Layout";
import ListVideo from "../components/ListVideo";
import dynamic from "next/dynamic";
import { useState, useEffect } from "react";
import { Container, Row, Col, Table, Input } from "reactstrap";

const Player = dynamic(() => import("react-player"), {
  ssr: false,
});

const Video = () => {
  const [play, setPlay] = useState(false);
  const [muted, setMuted] = useState(false);
  const [iconMuted, setIconMuted] = useState(false);
  const [iconPlay, setIconPlay] = useState(false);
  const [indexVideo, setIndexVideo] = useState(0);
  const [vol, setVol] = useState(0.5);
  const [volPercent, setVolPercent] = useState(50);

  const handleVol = (event) => {
    setVol(event.target.value);
    setVolPercent(event.target.value * 100);
  };

  const handleMute = () => {
    if (vol > 0) {
      setVol(0);
      setVolPercent(0);
    } else {
      setVol(0.5);
      setVolPercent(50);
    }
  };

  const handlePlay = () => {
    setPlay(!play);
    setIconPlay(!iconPlay);
  };

  const handleNext = () => {
    if (indexVideo == 5) {
      setIndexVideo(0);
    } else {
      setIndexVideo(indexVideo + 1);
    }
  };

  const handlePrev = () => {
    if (indexVideo == 0) {
      setIndexVideo(5);
    } else {
      setIndexVideo(indexVideo - 1);
    }
  };
  useEffect(() => {
    if (vol == 0) {
      setIconMuted(true);
      setMuted(true);
    } else {
      setIconMuted(false);
      setMuted(false);
    }
  }, [setIconMuted, setMuted, vol]);

  return (
    <Layout pageTitle="Covid Videos">
      <Header />
      <Container>
        <Row className="justify-content-center mb-2">
          <Col lg={6} md={8}>
            <div className="title text-center mb-5">
              <h3 className="font-weight-normal text-dark">
                <span className="text-warning">Video </span>
                <span className="text-danger">Covid-19</span>
              </h3>
              <p className="text-muted">
                Kumpulan video covid 19 dari (https://www.youtube.com/)
              </p>
            </div>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col>
            <Row className="justify-content-center">
              <Player
                url={ListVideo[indexVideo]}
                playing={play}
                volume={vol}
                muted={muted}
              />
              <div className="title text-center mt-2">
                <h3>
                  <a
                    onClick={() => handlePrev()}
                    className="fas fa-arrow-circle-left btn-play text-success"
                  ></a>
                  <a
                    onClick={() => handlePlay()}
                    className={
                      iconPlay
                        ? "fas fa-pause-circle btn-play text-warning"
                        : "fas fa-play-circle btn-play text-danger"
                    }
                  ></a>
                  <a
                    onClick={() => handleNext()}
                    className="fas fa-arrow-circle-right btn-play text-success"
                  ></a>
                </h3>
              </div>
              <div className="title text-center mt-2">
                <h4>
                  <a
                    onClick={() => handleMute()}
                    className={
                      iconMuted
                        ? "fas btn-play fa-volume-mute"
                        : "fas btn-play fa-volume-up"
                    }
                  ></a>
                  <Input
                    className="btn-play"
                    color="secondary"
                    type="range"
                    min={0}
                    max={1}
                    step={0.1}
                    value={vol}
                    onChange={(event) => handleVol(event)}
                  />
                  <a className="btn-play">{volPercent}%</a>
                </h4>
              </div>
            </Row>
          </Col>
        </Row>
        <Row className="justify-content-center mt-5">
          <Col lg={6} md={8}>
            <div className="title text-center mb-1">
              <h3 className="font-weight-normal text-muted">List Video</h3>
            </div>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col lg={6} md={8}>
            <Table dark hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Judul Video</th>
                </tr>
              </thead>
              <tbody style={{ cursor: "pointer" }}>
                <tr onClick={() => setIndexVideo(0)}>
                  <th scope="row">1</th>
                  <td>#CeritaAnak: Awas, Ada Virus Corona!</td>
                </tr>
                <tr onClick={() => setIndexVideo(1)}>
                  <th scope="row">2</th>
                  <td>Corona Virus (COVID-19) - Apakah Indonesia Aman?</td>
                </tr>
                <tr onClick={() => setIndexVideo(2)}>
                  <th scope="row">3</th>
                  <td>Video Animasi Pembelajaran COVID-19</td>
                </tr>
                <tr onClick={() => setIndexVideo(3)}>
                  <th scope="row">4</th>
                  <td>Bagaimana Virus Corona Menyerang Organ Tubuh Kita?</td>
                </tr>
                <tr onClick={() => setIndexVideo(4)}>
                  <th scope="row">4</th>
                  <td>5M Gerakan Pencegahan Covid 19</td>
                </tr>
                <tr onClick={() => setIndexVideo(5)}>
                  <th scope="row">6</th>
                  <td>Animasi cegah Covid - 19</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};

export default Video;
