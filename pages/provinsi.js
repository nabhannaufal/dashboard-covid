import axios from "axios";
import Link from "next/link";
import Layout from "../components/Layout";
import Header from "../components/Header";
import React from "react";
import { useRouter } from "next/dist/client/router";
import {
  Container,
  Row,
  Col,
  Card,
  CardTitle,
  CardSubtitle,
  CardBody,
  Table,
  Button,
} from "reactstrap";

const Provinsi = ({ data }) => {
  const router = useRouter();
  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <Layout pageTitle="Provinsi Indnesia">
      <Header />
      <section className="section" id="provinsi">
        <Container>
          <Row className="justify-content-center">
            <Col lg={6} md={8}>
              <div className="title text-center mb-5">
                <h3 className="font-weight-normal text-dark">
                  <span className="text-warning">Provinsi</span>
                </h3>
                <p className="text-muted">
                  Kasus Covid-19 Berdasarkan Provinsi di Indonesia
                </p>
              </div>
            </Col>
          </Row>
          <Row>
            {data.map((item, index) => {
              return (
                <>
                  <Col sm="4">
                    <Card body className="text-center mb-4">
                      <CardTitle tag="h5" key={index}>
                        {item.attributes.Provinsi}
                      </CardTitle>
                      <CardBody>
                        Jumlah Kasus Positif :{" "}
                        <span className="text-danger">
                          {item.attributes.Kasus_Posi}
                        </span>
                      </CardBody>
                      <Link
                        href={`${item.attributes.Provinsi}`}
                        key={index}
                        passHref
                      >
                        <Button>Detail</Button>
                      </Link>
                    </Card>
                  </Col>
                </>
              );
            })}
          </Row>
        </Container>
      </section>
    </Layout>
  );
};

export const getStaticProps = async () => {
  const provinsi = await axios.get(
    "https://api.kawalcorona.com/indonesia/provinsi/"
  );
  const { data } = provinsi;
  return {
    props: {
      data: data,
    },
  };
};

export default Provinsi;
