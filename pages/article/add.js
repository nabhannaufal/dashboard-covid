import axios from "axios";
import Layout from "../../components/Layout";
import Header from "../../components/Header";
import React, { useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Button,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useRouter } from "next/router";

const AddArticle = () => {
  const router = useRouter();
  const [title, setTitle] = useState("");
  const [subTitle, setSubTitle] = useState("");
  const [content, setContent] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/article/", {
        title: title,
        subtitle: subTitle,
        content: content,
      })
      .then(function () {
        router.push("/article");
      })
      .catch(function (error) {
        alert(error);
      });
  };
  return (
    <Layout pageTitle="Add Article">
      <Header />
      <section className="section" id="provinsi">
        <Container>
          <Row className="justify-content-center">
            <Col lg={6} md={8}>
              <div className="title text-center mb-5">
                <h3 className="font-weight-normal text-dark">
                  <span className="text-warning">Tambah Article</span>
                </h3>
              </div>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col sm="6">
              <Card body>
                <Form onSubmit={(e) => handleSubmit(e)}>
                  <FormGroup className="mb-2">
                    <Label>Title</Label>
                    <Input
                      type="text"
                      placeholder="Title"
                      onChange={(e) => setTitle(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup className="mb-2">
                    <Label>Sub Title</Label>
                    <Input
                      type="text"
                      placeholder="Sub Title"
                      onChange={(e) => setSubTitle(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup className="mb-2">
                    <Label>Content</Label>
                    <Input
                      type="textarea"
                      rows={5}
                      placeholder="Content"
                      onChange={(e) => setContent(e.target.value)}
                    />
                  </FormGroup>
                  <Button type="submit">Submit</Button>
                </Form>
              </Card>
            </Col>
          </Row>
        </Container>
      </section>
    </Layout>
  );
};

export default AddArticle;
