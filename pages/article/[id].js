import axios from "axios";
import Layout from "../../components/Layout";
import Header from "../../components/Header";
import React from "react";
import Link from "next/link";
import { useState } from "react";
import { useRouter } from "next/router";
import {
  Container,
  Row,
  Col,
  Card,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

const DetailArticle = ({ data }) => {
  const router = useRouter();
  const getData = data.article;
  const [id] = useState(getData.id);
  const [title, setTitle] = useState(getData.title);
  const [subTitle, setSubTitle] = useState(getData.subtitle);
  const [content, setContent] = useState(getData.content);

  const handleUpdate = (e) => {
    e.preventDefault();
    axios
      .put(`http://localhost:8000/api/article/${id}`, {
        title: title,
        subtitle: subTitle,
        content: content,
      })
      .then(function () {
        router.push("/article");
      })
      .catch(function (error) {
        alert(error);
      });
  };

  return (
    <Layout pageTitle="Detail Article">
      <Header />
      <section className="section" id="provinsi">
        <Container>
          <Row className="justify-content-center">
            <Col lg={6} md={8}>
              <div className="title text-center mb-5">
                <h3 className="font-weight-normal text-dark">
                  <span className="text-warning">Detail Article </span>
                </h3>
              </div>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col>
              <Card body>
                <Form onSubmit={(e) => handleUpdate(e)}>
                  <FormGroup className="mb-2">
                    <Label>Title</Label>
                    <Input
                      type="text"
                      placeholder="Title"
                      value={title}
                      onChange={(e) => setTitle(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup className="mb-2">
                    <Label>Sub Title</Label>
                    <Input
                      type="text"
                      placeholder="Sub Title"
                      value={subTitle}
                      onChange={(e) => setSubTitle(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup className="mb-2">
                    <Label>Content</Label>
                    <Input
                      type="textarea"
                      rows={5}
                      placeholder="Content"
                      value={content}
                      onChange={(e) => setContent(e.target.value)}
                    />
                  </FormGroup>
                  <Button type="submit">Submit</Button>
                </Form>
              </Card>
            </Col>
          </Row>
        </Container>
      </section>
    </Layout>
  );
};

export const getStaticProps = async (ctx) => {
  const id = ctx.params.id;
  const detailArticle = await axios.get(
    `http://localhost:8000/api/article/${id}`
  );
  const { data } = detailArticle;
  console.log("ngenttod", data);

  return {
    props: {
      data: data,
    },
  };
};

export const getStaticPaths = async () => {
  const daftarArticle = await axios.get("http://localhost:8000/api/article/");
  const { data } = daftarArticle.data;
  const paths = data.map((item) => ({
    params: { id: item.id.toString() },
  }));

  return { paths, fallback: false };
};

export default DetailArticle;
