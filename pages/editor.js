import dynamic from "next/dynamic";
import Header from "../components/Header";
import Layout from "../components/Layout";
import { Container, Row, Col } from "reactstrap";

const CKEditor = dynamic(() => import("../components/CkEditor"), {
  ssr: false,
});

const Editor = () => {
  return (
    <Layout pageTitle="Article Covid">
      <Header />
      <Container>
        <Row className="justify-content-center mb-2">
          <Col>
            <div className="title text-center mb-5">
              <h3 className="font-weight-normal text-dark">
                <span className="text-warning">Text </span>
                <span className="text-danger">Editor</span>
              </h3>
            </div>
          </Col>
        </Row>
        <Row>
          <CKEditor />
        </Row>
      </Container>
    </Layout>
  );
};

export default Editor;
