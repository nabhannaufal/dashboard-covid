import axios from "axios";
import Layout from "../components/Layout";
import Header from "../components/Header";
import React from "react";
import { useRouter } from "next/dist/client/router";
import { messageAction } from "../redux/actions";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardText,
  CardTitle,
  Input,
  Button,
  Form,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";

const Index = ({ data }) => {
  const router = useRouter();
  const nama = data[0].name;
  const positif = data[0].positif;
  const sembuh = data[0].sembuh;
  const meninggal = data[0].meninggal;
  const dirawat = data[0].dirawat;
  const pesan = useSelector((state) => state.pesan);
  const dispatch = useDispatch();
  const [message, setMessage] = useState("");
  const handleClick = (e) => {
    const kirimPesan = message;
    dispatch(messageAction(kirimPesan));
  };

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <Layout pageTitle="Dashboard Covid-19">
      <Header />
      <section className="section">
        <Container>
          <Row className="justify-content-center">
            <Col lg={6} md={8}>
              <div className="title text-center mb-5">
                <h3 className="font-weight-normal text-dark">
                  <span className="text-warning">{nama}</span>
                </h3>
                <p className="text-muted">
                  Kasus Covid-19 (https://kawalcorona.com/api/indonesia)
                </p>
              </div>
            </Col>
          </Row>
          <Row className="justify-content-center mb-4">
            <Col sm="3">
              <Card color="danger" body className="text-center text-white">
                <CardTitle tag="h5">Positif</CardTitle>
                <CardText>{positif}</CardText>
              </Card>
            </Col>
            <Col sm="3">
              <Card body color="success" className="text-center text-white">
                <CardTitle tag="h5">Sembuh</CardTitle>
                <CardText>{sembuh}</CardText>
              </Card>
            </Col>
          </Row>
          <Row className="justify-content-center mb-4">
            <Col sm="3">
              <Card body color="secondary" className="text-center text-white">
                <CardTitle tag="h5">Meninggal</CardTitle>
                <CardText>{meninggal}</CardText>
              </Card>
            </Col>
            <Col sm="3">
              <Card body color="primary" className="text-center text-white">
                <CardTitle tag="h5">Dirawat</CardTitle>
                <CardText>{dirawat}</CardText>
              </Card>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col lg={6} md={8}>
              <div className="title text-center mb-5">
                <h2 className="text-info">{pesan}</h2>
                <p className="text-muted">
                  <i>(komentar dengan redux)</i>
                </p>
                <Form>
                  <InputGroup>
                    <Input
                      name="pesan"
                      id="pesan"
                      placeholder="Masukan komentar anda!"
                      onChange={(event) => setMessage(event.target.value)}
                    />
                    <InputGroupAddon addonType="append">
                      <Button type="reset" onClick={() => handleClick()}>
                        Update!
                      </Button>
                    </InputGroupAddon>
                  </InputGroup>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </Layout>
  );
};

export default Index;

export const getStaticProps = async () => {
  const indo = await axios.get("https://api.kawalcorona.com/indonesia/");
  const { data } = indo;
  return {
    props: {
      data: data,
    },
  };
};
