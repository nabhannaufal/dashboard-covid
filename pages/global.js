import axios from "axios";
import Link from "next/link";
import Layout from "../components/Layout";
import Header from "../components/Header";
import React from "react";
import { Container, Row, Col, Card, CardTitle, Table } from "reactstrap";
import { useRouter } from "next/dist/client/router";

const Global = ({ data }) => {
  const router = useRouter;
  if (router.isFallback) {
    return <div>Loading...</div>;
  }
  return (
    <Layout pageTitle="Covid Global">
      <Header />
      <section className="section" id="global">
        <Container>
          <Row className="justify-content-center">
            <Col lg={6} md={8}>
              <div className="title text-center mb-5">
                <h3 className="font-weight-normal text-dark">
                  <span className="text-warning">Global</span>
                </h3>
                <p className="text-muted">
                  Kasus Covid-19 (https://kawalcorona.com/api/)
                </p>
              </div>
            </Col>
          </Row>
          <Row>
            {data.map((item, index) => {
              return (
                <>
                  <Col sm="4">
                    <Card body className="mb-4">
                      <CardTitle tag="h5" className="text-center" key={index}>
                        {item.attributes.Country_Region}
                      </CardTitle>
                      <Table>
                        <tbody>
                          <tr>
                            <th>Confirmed</th>
                            <td>:</td>
                            <td>{item.attributes.Confirmed}</td>
                          </tr>
                          <tr>
                            <th>Deaths</th>
                            <td>:</td>
                            <td>{item.attributes.Deaths}</td>
                          </tr>
                        </tbody>
                      </Table>
                    </Card>
                  </Col>
                </>
              );
            })}
          </Row>
        </Container>
      </section>
    </Layout>
  );
};

export const getStaticProps = async () => {
  const provinsi = await axios.get("https://api.kawalcorona.com/");
  const { data } = provinsi;
  return {
    props: {
      data: data,
    },
  };
};

export default Global;
