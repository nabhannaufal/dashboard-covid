import Header from "../components/Header";
import Layout from "../components/Layout";
import axios from "axios";
import { Container, Row, Col, Table, Button } from "reactstrap";
import { useRouter } from "next/router";

const Article = ({ data }) => {
  const daftarArticle = data.data;
  const router = useRouter();

  const handleDelete = (id) => {
    axios
      .delete(`http://localhost:8000/api/article/${id}`)
      .then(function () {
        router.push("/article");
      })
      .catch(function (error) {
        alert(error);
      });
  };

  return (
    <Layout pageTitle="Article Covid">
      <Header />
      <Container className="mb-5">
        <Row className="justify-content-center mb-2">
          <Col>
            <div className="title text-center mb-4">
              <h3 className="font-weight-normal text-dark">
                <span className="text-warning">Article </span>
                <span className="text-danger">Covid-19</span>
              </h3>
              <p className="text-muted">
                Kumpulan article covid 19 dari (https://kompas.com/)
              </p>
            </div>
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-start">
            <h6 className="font-weight-normal text-dark">
              <span className="text-warning">Article </span>
              <span className="text-danger">Covid-19</span>
            </h6>
          </Col>
          <Col className="d-flex justify-content-end">
            <Button href="/article/add">Tambah</Button>
          </Col>
        </Row>
        <Row className="justify-content-center mb-5">
          <Col>
            <Table hover small>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Judul Artikel</th>
                  <th>Sub Judul Artikel</th>
                  <th>Update terakhir</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {daftarArticle.map((item, index) => {
                  return (
                    <>
                      <tr>
                        <th scope="row">{index + 1}</th>
                        <td>{item.title}</td>
                        <td>{item.subtitle}</td>
                        <td>{item.updatedAt}</td>
                        <td>
                          <a
                            href={"/article/" + item.id}
                            className="fas fa-edit text-success"
                          >
                            {" "}
                          </a>
                          <a
                            onClick={() => handleDelete(item.id)}
                            className="fas fa-trash-alt text-danger edit-icon"
                          >
                            {" "}
                          </a>
                        </td>
                      </tr>
                    </>
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};

export default Article;

export const getStaticProps = async () => {
  const getArticle = await axios.get("http://localhost:8000/api/article/");
  return {
    props: {
      data: getArticle.data,
    },
  };
};
