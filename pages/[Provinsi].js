import axios from "axios";
import Layout from "../components/Layout";
import Header from "../components/Header";
import React from "react";
import Link from "next/link";
import { useRouter } from "next/dist/client/router";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Button,
  Table,
} from "reactstrap";

const DetailProvinsi = ({ namaProvinsi, detailProvinsi }) => {
  const cekProvinsi = (prov) => {
    return (prov.attributes.Provinsi = namaProvinsi);
  };

  const dataProvinsi = detailProvinsi.find(cekProvinsi);
  const nama = dataProvinsi.attributes.Provinsi;
  const positif = dataProvinsi.attributes.Kasus_Posi;
  const sembuh = dataProvinsi.attributes.Kasus_Semb;
  const meninggal = dataProvinsi.attributes.Kasus_Meni;
  const router = useRouter();

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <Layout pageTitle="Detail Provinsi">
      <Header />
      <section className="section" id="provinsi">
        <Container>
          <Row className="justify-content-center">
            <Col lg={6} md={8}>
              <div className="title text-center mb-5">
                <h3 className="font-weight-normal text-dark">
                  <span className="text-warning">
                    Kasus Covid-19 Berdasarkan Provinsi
                  </span>
                </h3>
                <p className="text-muted">Data Covid 19 di Provinsi {nama}</p>
              </div>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col sm="4">
              <Card body>
                <CardTitle tag="h5" className="text-center">
                  {nama}
                </CardTitle>
                <CardBody>
                  <Table>
                    <tbody>
                      <tr>
                        <th>Positif</th>
                        <td>:</td>
                        <td>{positif}</td>
                      </tr>
                      <tr>
                        <th>Sembuh</th>
                        <td>:</td>
                        <td>{sembuh}</td>
                      </tr>
                      <tr>
                        <th>Meninggal</th>
                        <td>:</td>
                        <td>{meninggal}</td>
                      </tr>
                    </tbody>
                  </Table>
                </CardBody>
                <Link href="/provinsi" passHref>
                  <Button color="warning">Kembali</Button>
                </Link>
              </Card>
            </Col>
          </Row>
        </Container>
      </section>
    </Layout>
  );
};

export const getStaticProps = async (ctx) => {
  const namaProvinsi = ctx.params.Provinsi;
  const daftarProvinsi = await axios.get(
    "https://api.kawalcorona.com/indonesia/provinsi/"
  );
  const { data } = daftarProvinsi;

  return {
    props: {
      namaProvinsi,
      detailProvinsi: data,
    },
  };
};

export const getStaticPaths = async () => {
  const daftarProvinsi = await axios.get(
    "https://api.kawalcorona.com/indonesia/provinsi/"
  );
  const { data } = daftarProvinsi;
  const paths = data.map((indonesia) => ({
    params: { Provinsi: indonesia.attributes.Provinsi.toString() },
  }));

  return { paths, fallback: false };
};

export default DetailProvinsi;
