import messageReducer from "./messageReducer";
import { combineReducers } from "redux";
import usernameReducer from "./usernameReducer";

const Reducers = combineReducers({
  pesan: messageReducer,
  newUser: usernameReducer,
});

export default Reducers;
